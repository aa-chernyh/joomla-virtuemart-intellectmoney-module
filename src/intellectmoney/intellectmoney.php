<?php

require_once("IntellectMoneyCommon/UserSettings.php");
require_once("IntellectMoneyCommon/LanguageHelper.php");
require_once("IntellectMoneyCommon/Order.php");
require_once("IntellectMoneyCommon/Customer.php");
require_once("IntellectMoneyCommon/Payment.php");
require_once("IntellectMoneyCommon/Result.php");
require_once("IntellectMoneyCommon/Status.php");

if (!defined('_VALID_MOS') && !defined('_JEXEC'))
    die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

if (!class_exists('vmPSPlugin'))
    require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');

class plgVmPaymentIntellectMoney extends vmPSPlugin {

    // instance of class
    public static $_this = false;

    function __construct(&$subject, $config) {
        parent::__construct($subject, $config);
        $jlang = JFactory::getLanguage();
        $jlang->load('plg_vmpayment_intellectmoney', JPATH_ADMINISTRATOR, NULL, TRUE);
        $this->_loggable = true;
        $this->tableFields = array_keys($this->getTableSQLFields());
        $this->_tablepkey = 'id';
        $this->_tableId = 'id';
        $varsToPush = array(
            'payment_logos' => array('../../../../plugins/vmpayment/intellectmoney/intellectmoney.png', 'char'),
            'eshopId' => array('', 'string'),
            'secretKey' => array('', 'string'),
            'tax' => array(1, 'int'),
            'deliveryTax' => array(1, 'int'),
            'statusCreated' => array('', 'char'),
            'statusCancelled' => array('', 'char'),
            'statusPaid' => array('', 'char'),
            'statusHolded' => array('', 'char'),
            'statusPartiallyPaid' => array('', 'char'),
            'statusRefunded' => array('', 'char'),
            'testMode' => array(0, 'int'),
            'holdMode' => array(0, 'int'),
            'holdTime' => array(72, 'int'),
            'expireDate' => array('', 'int'),
            'preference' => array('', 'string'),
            'successUrl' => array('', 'string'),
            'backUrl' => array('', 'string'),
            'group' => array(NULL, 'string'),
        );
        $this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
    }

    public function plgVmConfirmedOrder($cart, $order) {
        if (!($method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
            return null; // Another method was selected, do nothing
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        $this->includeOrderFileIfNotExists();

        $lang = JFactory::getLanguage();
        $lang->load('com_virtuemart', JPATH_ADMINISTRATOR);
        if (!$method->payment_currency) {
            $this->getPaymentCurrency($method);
        }
        $recipientCurrency = shopFunctions::getCurrencyByID($method->payment_currency, 'currency_code_3');
        $orderId = VirtueMartModelOrders::getOrderIdByOrderNumber($order['details']['BT']->order_number);

        $imUserSettings = $this->loadSettings($method, $order);
        $imOrder = \PaySystem\Order::getInstance(NULL, $orderId, NULL, $order['details']['BT']->order_total, NULL, $order['details']['BT']->order_shipment, $recipientCurrency, NULL, NULL);
        foreach ($order['items'] as $item) {
            $imOrder->addItem($item->product_final_price, $item->product_quantity, $item->order_item_name, $imUserSettings->getTax());
        }
        $imCustomer = \PaySystem\Customer::getInstance($order['details']['BT']->email, $order['details']['BT']->first_name . " " . $order['details']['BT']->last_name, $order['details']['BT']->phone_1);
        $imPayment = \PaySystem\Payment::getInstance($imUserSettings, $imOrder, $imCustomer);
        $imUserSettings->setMerchantUrl("https://merchant.intellectmoney.ru");
        $form = $imPayment->generateForm(true, true);

        return $this->processConfirmedOrderPaymentResponse(true, $cart, $order, $form, $this->renderPluginName($method, $order), 'P');
    }

    private function loadSettings($method, $order) {
        $imUserSettings = PaySystem\UserSettings::getInstance();
        $userSettingsParams = array();
        foreach ($imUserSettings->getNamesOfOrganizationParamsToSave() as $value) {
            $userSettingsParams[$value] = isset($method->$value) ? $method->$value : "";
        }
        if (empty($userSettingsParams['successUrl'])) {
            $userSettingsParams['successUrl'] = JROUTE::_(JURI::root() . 'index.php?option=com_virtuemart&view=orders&layout=details&order_number=' . $order['details']['BT']->order_number . '&order_pass=' . $order['details']['BT']->order_pass);
        }
        $imUserSettings->setParams($userSettingsParams);
        return $imUserSettings;
    }

    public function plgVmOnPaymentNotification() {
        $this->includeOrderFileIfNotExists();
        $orderModel = VmModel::getModel('orders');
        $order = $orderModel->getOrder($_REQUEST['orderId']);
        if (!isset($order)) {
            echo "OK";
            die;
        }

        $method = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id);
        if (!$this->selectedThisElement($method->payment_element)) {
            return FALSE;
        }

        $imUserSettings = $this->loadSettings($method, $order);
        $imOrder = \PaySystem\Order::getInstance($_REQUEST['paymentId'], $_REQUEST['orderId'], $_REQUEST['recipientOriginalAmount'], $_REQUEST['recipientAmount'], NULL, NULL, $_REQUEST['recipientCurrency'], NULL, $this->getImStatusFromCustomDataField($orderModel->getOrder($_REQUEST['orderId'])['history']));
        PaySystem\LanguageHelper::getInstance('ru');
        $imResult = \PaySystem\Result::getInstance($_REQUEST, $imUserSettings, $imOrder);
        $response = $imResult->processingResponse();
        if ($response->changeStatusResult) {
            $customOrderData = array(
                'comments' => $this->generateCustomDataField($_REQUEST['paymentId'], $_REQUEST['paymentStatus'], $orderModel->getOrder($_REQUEST['orderId'])['history']),
                'customer_notified' => true,
                'order_status' => $response->statusCMS,
            );
            $orderModel->updateStatusForOneOrder($_REQUEST['orderId'], $customOrderData, TRUE);
        }

        echo $imResult->getMessage();
        die;
    }

    private function includeOrderFileIfNotExists() {
        if (!class_exists('VirtueMartModelOrders')) {
            require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
        }
    }

    private function generateCustomDataField($invoiceId = NULL, $imStatus = NULL, $history = NULL) {
        if ($history) {
            if (empty($invoiceId)) {
                $invoiceId = $this->getInvoiceIdFromCustomDataField($history);
            }
            if (empty($imStatus)) {
                $imStatus = $this->getImStatusFromCustomDataField($history);
            }
        }
        return serialize(
            array(
                "paymentProcessor" => "IntellectMoney",
                "invoiceId" => $invoiceId,
                "imStatus" => $imStatus
            )
        );
    }

    private function getInvoiceIdFromCustomDataField($history) {
        $customFields = $this->getCustomFieldsArray($this->getLastImCustomDataField($history));
        return $customFields ? $customFields['invoiceId'] : 0;
    }

    private function getImStatusFromCustomDataField($history) {
        $customFields = $this->getCustomFieldsArray($this->getLastImCustomDataField($history));
        return $customFields ? $customFields['imStatus'] : PaySystem\Status::preCreated;
    }

    private function getCustomFieldsArray($customField) {
        if (!empty($customField)) {
            $customFields = unserialize($customField);
            if ($customFields["paymentProcessor"] == "IntellectMoney") {
                return $customFields;
            }
        }
        return false;
    }

    private function getLastImCustomDataField($history) {
        foreach (array_reverse($history) as $historyElem) {
            if ($this->getCustomFieldsArray($historyElem->comments)) {
                return $historyElem->comments;
            }
        }
        return false;
    }

    function plgVmOnPaymentResponseReceived(&$html) {
        // the payment itself should send the parameter needed;

        $virtuemart_paymentmethod_id = JRequest::getInt('pm', 0);

        $vendorId = 0;
        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return null; // Another method was selected, do nothing
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }

        if (!class_exists('VirtueMartModelOrders'))
            require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');

        $order_number = JRequest::getVar('on');
        $virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number);
        $payment_name = $this->renderPluginName($method);
        $html = '<table>' . "\n";
        $html .= $this->getHtmlRow('INTELLECTMONEY_PAYMENT_NAME', $payment_name);
        $html .= $this->getHtmlRow('INTELLECTMONEY_ORDER_NUMBER', $virtuemart_order_id);
        $html .= $this->getHtmlRow('INTELLECTMONEY_STATUS', JText::_('VMPAYMENT_INTELLECTMONEY_STATUS_SUCCESS'));

        $html .= '</table>' . "\n";

        if ($virtuemart_order_id) {
            if (!class_exists('VirtueMartCart'))
                require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
            // get the correct cart / session
            $cart = VirtueMartCart::getCart();

            // send the email ONLY if payment has been accepted
            if (!class_exists('VirtueMartModelOrders'))
                require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
            $order = new VirtueMartModelOrders();
            $orderitems = $order->getOrder($virtuemart_order_id);
            $cart->sentOrderConfirmedEmail($orderitems);
            $cart->emptyCart();
        }

        return true;
    }

    function plgVmDeclarePluginParamsPaymentVM3(&$data) {
        return $this->declarePluginParams('payment', $data);
    }

    function plgVmOnShowOrderBEPayment($virtuemart_order_id, $virtuemart_payment_id) {
        if (!$this->selectedThisByMethodId($virtuemart_payment_id)) {
            return null; // Another method was selected, do nothing
        }

        $db = JFactory::getDBO();
        $q = 'SELECT * FROM `' . $this->_tablename . '` ' . 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
        $db->setQuery($q);
        if (!($paymentTable = $db->loadObject())) {
            vmWarn(500, $q . " " . $db->getErrorMsg());
            return '';
        }
        $this->getPaymentCurrency($paymentTable);

        $html = '<table class="adminlist table">' . "\n";
        $html .= $this->getHtmlHeaderBE();
        $html .= $this->getHtmlRowBE('STANDARD_PAYMENT_NAME', $paymentTable->payment_name);
        $html .= $this->getHtmlRowBE('STANDARD_PAYMENT_TOTAL_CURRENCY', $paymentTable->payment_order_total . ' ' . $paymentTable->payment_currency);
        $html .= '</table>' . "\n";
        return $html;
    }

    function getCosts(VirtueMartCart $cart, $method, $cart_prices) {
        return 0;
    }

    protected function checkConditions($cart, $method, $cart_prices) {
        return true;
    }

    function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {
        return $this->onStoreInstallPluginTable($jplugin_id);
    }

    public function plgVmOnSelectCheckPayment(VirtueMartCart $cart) {
        return $this->OnSelectCheck($cart);
    }

    public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
        return $this->displayListFE($cart, $selected, $htmlIn);
    }

    public function plgVmonSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
        return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
    }

    function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {
        if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
            return null; // Another method was selected, do nothing
        }
        if (!$this->selectedThisElement($method->payment_element)) {
            return false;
        }
        $this->getPaymentCurrency($method);

        $paymentCurrencyId = $method->payment_currency;
    }

    function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array()) {
        return $this->onCheckAutomaticSelected($cart, $cart_prices);
    }

    public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
        $this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
    }

    function plgVmonShowOrderPrintPayment($order_number, $method_id) {
        return $this->onShowOrderPrint($order_number, $method_id);
    }

    function plgVmDeclarePluginParamsPayment($name, $id, &$data) {
        return $this->declarePluginParams('payment', $name, $id, $data);
    }

    function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
        return $this->setOnTablePluginParams($name, $id, $table);
    }

    protected function displayLogos($logo_list) {
        $img = "";

        if (!(empty($logo_list))) {
            $url = JURI::root() . str_replace('\\', '/', str_replace(JPATH_ROOT, '', dirname(__FILE__))) . '/';
            if (!is_array($logo_list))
                $logo_list = (array) $logo_list;
            foreach ($logo_list as $logo) {
                $alt_text = substr($logo, 0, strpos($logo, '.'));
                $img .= '<img align="middle" src="' . $url . $logo . '"  alt="' . $alt_text . '" /> ';
            }
        }
        return $img;
    }

    protected function getVmPluginCreateTableSQL() {
        return $this->createTableSQL('Payment IntellectMoney Table');
    }

    function getTableSQLFields() {
        $SQLfields = array(
            'id' => 'tinyint(1) unsigned NOT NULL AUTO_INCREMENT',
            'virtuemart_order_id' => 'int(11) UNSIGNED DEFAULT NULL',
            'order_number' => 'char(32) DEFAULT NULL',
            'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED DEFAULT NULL',
            'payment_name' => 'char(255) NOT NULL DEFAULT \'\' ',
            'payment_order_total' => 'decimal(15,2) NOT NULL DEFAULT \'0.00\' ',
            'payment_currency' => 'char(3) '
        );

        return $SQLfields;
    }

}
